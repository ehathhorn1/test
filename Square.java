
public class Square extends Shape {

	public Square(int sideLength) {
		
		super(sideLength);
	}
	
	public double area()
	{
		return value * value;
	}
}

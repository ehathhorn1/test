
public class AbstraceTest {

	public static void main(String[] args) {

		Shape shape;
		
		shape = new Circle(3);
		System.out.println("Area of a circle = " + shape.area());
		
		shape = new Square(4);
		System.out.println("Area of a square = " + shape.area());
		
	}

}


public abstract class Shape {

	int value;
	
	public Shape(int a) 
	{
		value = a;
	}
	
	public abstract double area();
	
}
